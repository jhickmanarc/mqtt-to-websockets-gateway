import json
import os
import hashlib

def authenticate(user, passwd):
    wd = os.path.dirname(os.path.realpath(__file__))
    path = wd + "/sessionkeys/" + user + ".json"
    r = False
    w = False
    if os.path.isfile(path):
        fp = open(path)
        ctext = fp.read()
        fp.close()
        sd = json.loads(ctext)
        h = hashlib.sha224(passwd).hexdigest()
        if sd['hash'] == h:
            r = sd['read']
            w = sd['write']
    return [r, w]