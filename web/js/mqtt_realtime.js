function MQTTrealtime(url, user_name, passwd) {
	var map = {};
	var listeners = [];
	var topics = [];
	var ready_handlers = [];
	var ws = null;
	
	var authentication_failure = function(){};
	var write_denied = function(){};
	
	function authenticate() {
		if (ws != null) {
			ws.send(":a " + user_name + "::" + passwd + "\n");
		}
	}
	
	/*
	 * Public interface
	 */
	
	this.subscribe = function(topic, fctn) {
		/*
		 * Register a call-back to listen to an MQTT Topic.
		 * DOES NOT SUPPORT WILDCARDS!
		 */
		if (!map.hasOwnProperty(topic)) {
			map[topic] = [];
		}
		map[topic].push(fctn);
	}
	
	this.listener = function(fctn) {
		/*
		 * Listeners get ALL incoming messages,
		 * use if listening to wild-card topics
		 * is needed.
		 */
		listeners.push(fctn);
	}
	
	this.topic = function(topic) {
		/*
		 * Set topics that have no specific call-back,
		 * suitable for wild card topics. 
		 */
		topics.push(topic);
		if (ws != null) {
			ws.send(":s " + topic + "\n");
		}
	}
	
	this.ready_listener = function(fctn) {
		/*
		 * Call-back function to be informed
		 * when the connection is set up.
		 */
		ready_handlers.push(fctn);
	}
	
	this.connect = function() {
		/*
		 * After registering any call-backs and topics
		 * actually connect to the server and
		 * tell it what Topics we are interested in.
		 */
		ws = new WebSocket(url);
		
		ws.onmessage = function(d) {
			if (d.data == "AUTHENTICATION_FAILED") {
				authentication_failure();
				return;
			}
			if (d.data == "WRITE_ACCESS_DENIED") {
				write_denied();
				return;
			}
			try {
				var data = JSON.parse(d.data);
				for (var i = 0; i < listeners.length; i++) {
					listeners[i](data);
				}
				if (map.hasOwnProperty(data.topic)) {
					for (var i = 0; i < map[data.topic].length; i++) {
						map[data.topic][i](data.message);
					}
				}
			}
			catch(e) {
				console.log(e);
			}
		}
		
		ws.onopen = function(e) {
			for (var topic in map) {
				ws.send(":s " + topic +"\n");
			}
			for (var i = 0; i < topics.length; i++) {
				ws.send(":s " + topics[i] +"\n");
			}
			authenticate();
			for (var i = 0; i < ready_handlers.length; i++) {
				ready_handlers[i]();
			}
		}
	}
	
	this.set_authentication_failure = function(fctn) {
		/*
		 * Set handler in the event that authentication fails.
		 */
		authentication_failure = fctn;
	}
	
	this.set_write_denied = function(fctn) {
		/*
		 * Set handler for the write denied error condition.
		 */
		write_denied = fctn;
	}
	
	this.reset_authentication = function(u, p) {
		/*
		 * Use to change the user-name and password
		 * and try authentication again.
		 */
		user_name = u;
		passwd = passwd;
		authenticate();
	}
	
	this.send = function(topic, payload) {
		/*
		 * Send a MQTT message.
		 * If return false the connection was not ready.
		 */
		if (ws != null && ws.readyState === ws.OPEN) {
			ws.send(topic + ':' + JSON.stringify(payload));
			return true;
		}
		return false;
	}
	
	return this;
}