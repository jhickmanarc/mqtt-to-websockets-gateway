#!/usr/bin/python
import paho.mqtt.client as mqtt
from thread import *
import threading
import json
import sys

from session import authenticate
from configuration import get_config

def on_connect(client, userdata, flags, rc):
    #client.subscribe("$SYS/#")
    return

def on_message(client, userdata, msg):
    global read_access
    if read_access:
        print json.dumps({"topic": msg.topic, "message": msg.payload})
    sys.stdout.flush()
    return

def mqtt_handler(client):
    """ Thread to listen for MQTT messages. """
    global running
    while running:
        client.loop()

if __name__ == '__main__':
    """
    Main application.
    """
    global running, read_access, write_access
    running = True
    read_access = False
    write_access = False
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    config = get_config("/config/settings.json")
    client.connect(config['host'], config['port'], config['keepalive'], config['bind_address'])
    if 'username' in config and 'password' in config:
        client.username_pw_set(config['username'], config['password'])
    start_new_thread(mqtt_handler, (client,))
    while running:
        command = raw_input('')
        if len(command) > 1 and command[0] == ':':
            """ Command """
            c, p = command.split(' ')
            if c == ":s":
                # SUBSCRIBE
                client.subscribe(p)
            if c == ":u":
                # Unsubscribe
                client.unsubscribe(p)
            if c == ":q":
                running = False
            if c == ":a":
                # Authenticate
                user, passwd = p.split("::")
                read_access, write_access = authenticate(user, passwd)
                if read_access == False and write_access == False:
                    print "AUTHENTICATION_FAILED"
                    sys.stdout.flush()
        else:
            split = command.find(':')
            if split != -1:
                topic = command[:split]
                payload = command[split + 1:]
                if write_access:
                    client.publish(topic, payload)
                else:
                    print "WRITE_ACCESS_DENIED"
                    sys.stdout.flush()