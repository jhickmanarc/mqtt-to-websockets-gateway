import json
import os

def get_config(file_name):
    wd = os.path.dirname(os.path.realpath(__file__))
    fp = open(wd + file_name)
    ctext = fp.read()
    fp.close()
    configuration = json.loads(ctext)
    return configuration